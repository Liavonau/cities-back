package com.app.cities.service;

import com.app.cities.entity.City;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CityService {

  City findCity(Long id);

  Page<City> findCities(String searchNameString, Pageable pageable);

  City updateCity(City city);
}
