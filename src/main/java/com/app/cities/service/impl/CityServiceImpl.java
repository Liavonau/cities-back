package com.app.cities.service.impl;

import com.app.cities.entity.City;
import com.app.cities.exception.NotFoundException;
import com.app.cities.repository.CityRepository;
import com.app.cities.service.CityService;
import io.micrometer.common.util.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository cityRepository;

    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public City findCity(Long id) {
        return cityRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("CITY_NOT_FOUND"));
    }

    @Override
    public Page<City> findCities(String searchNameString, Pageable pageable) {
        if (StringUtils.isEmpty(searchNameString)) {
            return cityRepository.findAll(pageable);
        } else {
            searchNameString = searchNameString.trim();
        }
        return StringUtils.isEmpty(searchNameString) ? cityRepository.findAll(pageable)
                : cityRepository.findByNameContainingIgnoreCase(searchNameString, pageable);
    }

    @Override
    public City updateCity(City city) {
        // New city shouldn't be saved
        cityRepository.findById(city.getId())
                .orElseThrow(() -> new NotFoundException("CITY_NOT_FOUND"));
        return cityRepository.save(city);
    }
}
