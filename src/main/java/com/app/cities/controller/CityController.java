package com.app.cities.controller;

import com.app.cities.dto.CityDto;
import com.app.cities.entity.City;
import com.app.cities.service.CityService;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController()
@RequestMapping("/api/cities")
@CrossOrigin
public class CityController {

  private static final int DEFAULT_PAGE_SIZE = 8;

  private final CityService cityService;
  private final ModelMapper modelMapper;

  public CityController(final CityService cityService, final ModelMapper modelMapper) {
    this.cityService = cityService;
    this.modelMapper = modelMapper;
  }

  @GetMapping
  public ResponseEntity<Page<CityDto>> getCities(
          @RequestParam(value = "search", required = false) String searchNameString,
          @PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
    Page<CityDto> cityDtos = cityService.findCities(searchNameString, pageable)
            .map(city -> modelMapper.map(city, CityDto.class));
    return ok(cityDtos);
  }

  @GetMapping("/{id}")
  public ResponseEntity<CityDto> getCity(@PathVariable Long id) {
    CityDto cityDto = modelMapper.map(cityService.findCity(id), CityDto.class);
    return ok(cityDto);
  }

  @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CityDto> updateCity(@RequestBody CityDto cityDto) {
    City city = modelMapper.map(cityDto, City.class);
    CityDto cityDtoUpdated = modelMapper.map(cityService.updateCity(city), CityDto.class);
    return ok(cityDtoUpdated);
  }
}
