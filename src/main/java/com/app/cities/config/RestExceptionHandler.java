package com.app.cities.config;

import com.app.cities.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class RestExceptionHandler {

  @ExceptionHandler(NotFoundException.class)
  protected ResponseEntity<ApiError> handleNotAdminException(NotFoundException ex) {
    String message = "NOT_FOUND_EXCEPTION";
    return buildResponseEntity(new ApiError(HttpStatus.NOT_FOUND, message, ex));
  }

  private ResponseEntity<ApiError> buildResponseEntity(ApiError apiError) {
    return new ResponseEntity(apiError, apiError.getStatus());
  }
}
