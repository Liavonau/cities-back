CREATE SEQUENCE city_id_seq;

CREATE TABLE public.city
(
    id               BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name             CHARACTER VARYING (64) NOT NULL,
    photo            CHARACTER VARYING (1024) NOT NULL
);

COPY city (id, name, photo) FROM '/var/lib/postgresql/csvs/cities.csv' DELIMITER ',' CSV HEADER;