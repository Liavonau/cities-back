package com.app.cities;

import com.app.cities.dto.CityDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class CitiesTests {

	@Autowired
	private MockMvc mvc;

	@Test
	void getCitiesSearchStringNull() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/api/cities").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
                .andExpect(jsonPath("$.size", equalTo(8)))
                .andExpect(jsonPath("$.totalElements", equalTo(1000)))
                .andExpect(jsonPath("$.content", hasSize(8)));
	}

	@Test
	void getCitiesSearchStringEmpty() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/api/cities?search=").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
                .andExpect(jsonPath("$.size", equalTo(8)))
                .andExpect(jsonPath("$.totalElements", equalTo(1000)))
                .andExpect(jsonPath("$.content", hasSize(8)));
	}

	@Test
	void getCitiesSearchStringNotEmpty() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/api/cities?search=Mana&size=4").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
                .andExpect(jsonPath("$.size", equalTo(4)))
                .andExpect(jsonPath("$.totalElements", equalTo(3)))
                .andExpect(jsonPath("$.content", hasSize(3)))
                .andExpect(jsonPath("$.content[0].name", containsString("Mana")))
                .andExpect(jsonPath("$.content[1].name", containsString("Mana")))
                .andExpect(jsonPath("$.content[2].name", containsString("Mana")));
	}

	@Test
	void getCityById() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/api/cities/537").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", equalTo(537)))
				.andExpect(jsonPath("$.name", containsString("Managua")));
	}

	@Test
	void getCityByIdDoesNotExist() throws Exception {
		mvc.perform(MockMvcRequestBuilders.get("/api/cities/1537").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isNotFound())
				.andExpect(jsonPath("$.message", equalTo("NOT_FOUND_EXCEPTION")))
				.andExpect(jsonPath("$.debugMessage", equalTo("CITY_NOT_FOUND")));
	}

	@Test
	void updateCity() throws Exception {
		CityDto cityDto = getCityDto(537, "Managua_updated", "https://updated_url");
		mvc.perform(MockMvcRequestBuilders.put("/api/cities")
						.contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(cityDto))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", equalTo(537)))
				.andExpect(jsonPath("$.name", equalTo("Managua_updated")));
		mvc.perform(MockMvcRequestBuilders.get("/api/cities?search=Managua&size=4").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.size", equalTo(4)))
				.andExpect(jsonPath("$.totalElements", equalTo(1)))
				.andExpect(jsonPath("$.content", hasSize(1)))
				.andExpect(jsonPath("$.content[0].name", equalTo("Managua_updated")));
	}

	@Test
	void updateCityIdDoesNotExist() throws Exception {
		CityDto cityDto = getCityDto(1537, "Managua_updated", "https://updated_url");
		mvc.perform(MockMvcRequestBuilders.put("/api/cities")
						.contentType(MediaType.APPLICATION_JSON)
						.content(asJsonString(cityDto))
						.accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.message", equalTo("NOT_FOUND_EXCEPTION")))
				.andExpect(jsonPath("$.debugMessage", equalTo("CITY_NOT_FOUND")));
	}

	private CityDto getCityDto(long id, String name, String photo) {
		return CityDto.builder().id(id).name(name).photo(photo).build();
	}

	private static String asJsonString(final Object obj) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
